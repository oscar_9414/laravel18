@extends('layouts.app')
@section('title','Lista de Usuarios')
@section('content')
<style type="text/css">
.alert{
    padding: 10px;
    background-color: #faa;
    color: white;
    margin: 4px;
}
a{
    text-decoration: none;
    color: blue;
}

</style>
<h1>Editar usuario</h1>
<form method="post" action="/users/{{ $user->id }}">
    {{ csrf_field()}}
    {{-- genra un input oculto --}}
    <input type="hidden" name="_method" value="PUT">
    <label>Nombre:</label>
    <input type="text" name="name"
    value="{{ old('name') ? old('name') : $user->name }}">
    <br>
    <div class="alert alert-danger">
        {{ $errors->first('name')}}
        {{-- mostrará un mensaje en caso de no cumplir los requisitos --}}
    </div>
    <label>email:</label>
    <input type="email" name="email"
    value="{{ old('email') ? old('email') : $user->email }}">
    <br>
    <div class="alert alert-danger">
        {{ $errors->first('email')}}
    </div>
    <input type="submit" value="guardar">
</form>
<a href="/users">Inicio</a>
@endsection

