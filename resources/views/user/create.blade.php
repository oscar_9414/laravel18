@extends('layouts.app')
@section('title','Lista de Usuarios')
@section('content')
<style type="text/css">
.alert{
    padding: 10px;
    background-color: #faa;
    color: white;
    margin: 4px;
}
</style>
<h1>Alta de usuarios</h1>
<hr>
{{--
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
--}}
@if ($errors->any())
<div class="alert alert-danger">
 <h5>Se han producido errores</h5>
</div>
@endif
<form method="post" action="/users">
    {{ csrf_field()}}
    {{-- genra un input oculto --}}
    <label>Nombre:</label>
    <input type="text" name="name" value="{{ old('name') }} ">
    <br>
    <div class="alert alert-danger">
        {{ $errors->first('name')}}
    </div>
    <label>email:</label>
    <input type="email" name="email" value="{{ old('email') }}">
    <br>
    <div class="alert alert-danger">
        {{ $errors->first('email')}}
    </div>
    <?php
    $example = ['rojo','azul','verde']
    ?>
    <select name="color">
        @foreach ($example as $item)
        <option value="{{ $item}}"
        {!! old('color') == $item ? 'selected = "selected"' : '' !!}>
        {{ $item }}
    </option>
    @endforeach
</select>
<br>
<label>Contraseña:</label>
<input type="password" name="password">
<br>
<div class="alert alert-danger">
    {{ $errors->first('password')}}
</div>

<input type="submit" value="nuevo">
</form>

@endsection

