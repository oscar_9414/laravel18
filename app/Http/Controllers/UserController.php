<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::all();
        $users = User::paginate(2);
        // return $users;
        // return $users;
        // return "Lista de usuarios";
        // //NOTA: en karavek kas funciones incorporadas se denominan "Helpers", view es una de ella.
        // $users = ['Oscar','Ana','María','Javi','Sandra'];

        // $users = array();

        return view('user.index',['users'=> $users]);
        //buisca el fichero:
        //resources/views/user/index.php
        //resources/views/user/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//en este objeto se recibe todo lo de $_request como en el php tradicional
    {
        //validación
        // $rules = [
        //     'name'=>'required',
        //     'email'=>'required',
        //     'password'=>'required',
        // ];
        // $request->validate($rules);

        $request->validate([
            'name'=>'required|min:5',
            'email'=>'required|unique:users|max:255|email',//valida que el correo sea correcto
            'password'=>'required|max:255',
            'color'=>'required',
        ]);
        // dd($request->name);
        // dd($request->all());
        // $user =  new User;
        // $user->name = $request->input('name');
        // $user->email = $request->input('email');
        // $user->password = bcrypt($request->input('password'));9
        // $user->remember_token = str_random(10);
        // $user->save();//una manera de crear un usuario
        // // return "Guardar usuarios";
        // $user =  User::create($request->all());//OPCION 2

        $user = new User();//opcion 3
        $user->fill($request->all());//opcion 3
        $user->save();//opcion 3

        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)//laravel hará el fin si que lo haga yo
    {
        // $user = User::find($id);
        // return view('user.show',[
        //     'user'=>$user
        // ]);
        // $user = User::find($id);
        // if ($user == null) {
        //     abort(404,'Prohibido!!!!');
        // }

        // $user = USer::findOrFail($id);
        return view('user.show',[
            'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // return "craer formulario";
        $user = User::findOrFail($id);
        return view('user.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|min:4',
            'email'=>'required|unique:users,email,$id,id|max:255|email',//valida que el correo sea correcto
        ]);
        // return "falta guardar";
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();

        return redirect('/users/'.$user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return "Borrar usuarios";
        // $user =  User::findOrFail($id);//opcion 1
        // $user->delete();//opcion 1


        User::destroy($id);//opcion 2
        return back();

    }
    public function especial()
    {
        $users= User::where('id','>=',70)->where('id','<=',80)->get();
        dd($users);
        return redirect('/users');

    }
}


/*https://styde.net/laravel-5/*/
